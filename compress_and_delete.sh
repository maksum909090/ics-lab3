#!/bin/bash

# Check if the directory path and days threshold are provided as command-line arguments
if [ $# -ne 2 ]; then
  echo "Usage: $0 <directory_path> <days_threshold>"
  exit 1
fi

# Read the directory path and days threshold from command-line arguments
directory_path="$1"
days_threshold="$2"

# Create a timestamp to include in the archive filename
timestamp=$(date +"%Y%m%d%H%M%S")

# Create the archive filename with timestamp
archive_filename="$directory_path/archive_$timestamp.tar.gz"

# Use find to locate files older than the specified threshold and create the archive using tar
find "$directory_path" -type f -mtime +"$days_threshold" -exec tar -rvf "$archive_filename" {} \;

# Check if the archive was created successfully
if [ -f "$archive_filename" ]; then
  echo "Archive created: $archive_filename"

  # Use find to locate and delete files older than the specified threshold using rm
  find "$directory_path" -type f -mtime +"$days_threshold" -exec rm {} +

  echo "Old files deleted from $directory_path"
else
  echo "No files older than $days_threshold days found!"
fi
